var swiper = new Swiper(".mySwiper", {
  slidesPerView: 3,
  spaceBetween: 60,
  loop: true,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  autoplay: {
    delay: 1000,
  },
  breakpoints: {
    // when window width is >= 320px
    992: {
      slidesPerView: 3,
    },
    720: {
      slidesPerView: 2,
    },
    320: {
      slidesPerView: 1,
    },
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
});
$('#anylatic_text2').hide();
$('#anylatic_btn1, #anylatic_btn2').click(function(e) {
  e.preventDefault();
  $('#anylatic_btn1, #anylatic_btn2').removeClass('active');
  $(this).addClass('active');
  $('#anylatic_img1, #anylatic_img2, #anylatic_text2, #anylatic_text1').hide();

  if($(this).attr('id') == 'anylatic_btn1') {
    $('#anylatic_img1,#anylatic_text1').show();
  }
  if($(this).attr('id') == 'anylatic_btn2') {
    $('#anylatic_img2,#anylatic_text2').show();
  }
});